import Vue from 'vue'
import axios from 'axios'

const client = axios.create({
  baseURL: 'http://localhost/carmudi-exam-rlaig/web/index.php/api/',
  json: true
})

export default {
  async execute (method, resource, data) {
    return client({
      method,
      url: resource,
      data
    }).then(req => {
      return req.data
    })
  },
  getPosts () {
    return this.execute('get', '/vehicles')
  },
  getPost (id) {
    return this.execute('get', `/vehicles/${id}`)
  },
  createPost (data) {
    return this.execute('post', '/vehicles', data)
  },
  updatePost (id, data) {
    return this.execute('put', `/vehicles/${id}`, data)
  },
  deletePost (id) {
    return this.execute('delete', `/vehicles/${id}`)
  }
}