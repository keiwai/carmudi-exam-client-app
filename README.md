# Carmudi Exam Client Single Page App by R.Laig

## Setup
 - Install using _npm install_
 - Configure _/src/api.js_ for the proper API base url (default: http://localhost/carmudi-exam-rlaig/web/index.php/api/)

## Run App
 - Use npm to run _npm run dev_ or
 - Use npm to build the app _npm run build_ then open _app.html_